export const environment = {
  production: true,
  firebase: {
    apiKey: 'AIzaSyD4E6Z1uewMYZOzm8Vb8-hy0LHbdC1YJvc',
    authDomain: 'localhost',
    databaseURL: 'https://diary-ca1e1.firebaseio.com/',
    projectId: 'diary-ca1e1',
    storageBucket: 'gs://diary-ca1e1.appspot.com/',
    messagingSenderId: null
  }
};
