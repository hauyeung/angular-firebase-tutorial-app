// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyD4E6Z1uewMYZOzm8Vb8-hy0LHbdC1YJvc',
    authDomain: 'localhost',
    databaseURL: 'https://diary-ca1e1.firebaseio.com/',
    projectId: 'diary-ca1e1',
    storageBucket: 'gs://diary-ca1e1.appspot.com/',
    messagingSenderId: null
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
